
#include <Cubit/Libraries/Threading/Queue.hpp>
#include <iostream>
#include <thread>
#include <atomic>
#include <boost/bind.hpp>
#include <boost/function.hpp>

namespace th = Cubit::Libraries::Threading;

std::atomic_bool run = true;

int main() {
    th::Queue queue;
    std::thread processingThread(boost::bind<void>([](th::Queue::Dequeue dequeue) {
        std::cout << "thread start" << std::endl;
        while (run) {
            dequeue.dequeue().process();
        }
        std::cout << "thread end" << std::endl;
    }, queue.dequeue()));
    
    
    for (int i = 0; i < 10; ++i) {
        th::Event startEvent;
        
        th::Event event;
        queue.enqueue([&]() {
//            std::cout << "enqueue0" << std::endl;
            event.trigger();
        }, {startEvent});
        auto event1 = queue.enqueue([]() {
//            std::cout << "enqueue1" << std::endl;
        }, {event});
        auto event21 = queue.enqueue([]() {
//            std::cout << "enqueue21" << std::endl;
        }, {event1});
        auto event22 = queue.enqueue({[]() {
//            std::cout << "enqueue22" << std::endl;
        }}, {event1});
        auto event3 = queue.enqueue({[]() {
//            std::cout << "enqueue3" << std::endl;
        }}, {event21, event22});
        
        auto event4 = queue.enqueue({[]() {
//            std::cout << "enqueue4_wait3" << std::endl;
        }}, {event3});
        auto event5 = queue.enqueue({[]() {
//            std::cout << "enqueue5_wait1" << std::endl;
        }}, {event1});
        
        auto endEvent = queue.enqueue({[]() {
//            std::cout << "enqueue_last_wait_last" << std::endl;
        }}, {event4, event5});

//        std::cout << "start trigger" << std::endl;
        startEvent.trigger();
        endEvent.wait();
//        std::cout << "done!" << std::endl;
        if (!(i % 1000)) {
            std::cout << i << std::endl;
        }
    }
    std::cout << "done!" << std::endl;
    run = false;
    queue.null();
    processingThread.join();
}