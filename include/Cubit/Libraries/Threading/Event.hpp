#pragma once

#include <memory>
#include <boost/function.hpp>

namespace Cubit::Libraries::Threading{
    class Event{
        class IMPL;
        std::shared_ptr<IMPL> impl;
    public:
        Event();
        
        void wait();
        
        void trigger();
        
        void registerCallback(boost::function<void()> callback);
    };
}