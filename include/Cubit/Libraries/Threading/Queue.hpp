#pragma once

#include <Cubit/Libraries/Threading/Event.hpp>

namespace Cubit::Libraries::Threading {
    class Queue {
        class IMPL;
        
        std::shared_ptr<IMPL> impl;
    public:
        
        class Item {
            class IMPL;
            
            std::shared_ptr<IMPL> impl;
        
        public:
            Item(boost::function<void()> function, std::vector<Event> waitEvents = {});
            
            void whenReady(std::function<void()> callback);
            
            bool ready();
            
            Event event();
            
            void process();
        };
        
        
        Queue();
        
        Event enqueue(boost::function<void()> function, std::vector<Event> waitEvents = {});
        
        
        class Dequeue {
            class IMPL;
            
            std::shared_ptr<IMPL> impl;
        public:
            Dequeue(std::weak_ptr<Queue::IMPL> ptr);
            
            Item dequeue();
        };
        
        Dequeue dequeue();
    
        void null();
    };
}