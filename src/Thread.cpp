#include "Cubit/Libraries/Threading/Thread.hpp"
#include <Cubit/Libraries/Exceptions/Exceptions.hpp>

#include <atomic>
#include <thread>
#include <memory>
#include <Cubit/Libraries/Threading/Thread.hpp>
#include <Cubit/Libraries/Logging/Log.hpp>
#include <boost/thread.hpp>


namespace Cubit::Libraries::Threading {
    
    class Thread::IMPL {
        std::atomic_bool hasFunction = false;
        std::function<void()> function;
        
        std::atomic_bool started = false;
        std::thread internalThread;
        
        std::atomic_uint64_t threadID = std::numeric_limits<uint64_t>::max();
        std::atomic_bool joined = false;
        
        std::atomic_bool shouldTerminateAtException = true;
        std::vector<std::function<void(std::exception)>> uncaughtExceptionHandlers;
        
        std::weak_ptr<IMPL> selfPtr;
    public:
        
        IMPL();
        
        explicit IMPL(std::function<void()> function);
        
        ~IMPL();
        
        void setFunction(std::function<void()> function);
        
        void start();
        
        bool joinable();
        
        void join();
        
        uint64_t id();
        
        void setSelfPtr(std::shared_ptr<IMPL> ptr);
        
        void terminateAtException(bool shouldTerminate);
        
        void registerThreadSpecificUncaughtExceptionHandler(std::function<void(std::exception exception)> handler);
    };
    
    class OutOfThreadIDs : public Cubit::Libraries::Exceptions::FatalError {
    public:
        OutOfThreadIDs(const char* function, const char* file, int linenum, const std::string& message,
                       std::string errorType = "OutOfThreadIDs");
    
    public:
    
    };
    
    static thread_local Thread thisThread;
    static std::vector<Thread> runningThreads;
    static std::mutex vectorMutex;
    
    static std::vector<std::function<void(std::exception)>> uncaughtExceptionHandlers;
    
    static std::atomic_uint64_t nextID = 0;
//    static thread_local uint64_t thisThreadID = std::numeric_limits<uint64_t>::max();
    
    static uint64_t generateThreadID() {
        CUBIT_STACKTRACE
        CUBIT_SYNCHRONIZED_SCOPE
        if (nextID == std::numeric_limits<uint64_t>::max()) {
            // how? that's like 18 quintillion threads created
            throw OutOfThreadIDs(CUBIT_EXCEPTION_INFO, "ID limit reached");
        }
        return nextID++;
    }
    
    Thread::IMPL::IMPL() {
        CUBIT_STACKTRACE
    }
    
    Thread::IMPL::IMPL(std::function<void()> function) : IMPL() {
        CUBIT_STACKTRACE
        setFunction(function);
    }
    
    Thread::IMPL::~IMPL() {
        CUBIT_STACKTRACE
        if (internalThread.joinable()) {
            // can only happen when the thread is stopping
            // after all Thread objects go out of scope and it was not joined
            internalThread.detach();
        }
    }
    
    void Thread::IMPL::setFunction(std::function<void()> function) {
        CUBIT_STACKTRACE
        hasFunction = true;
        this->function = function;
    }
    
    void Thread::IMPL::start() {
        CUBIT_STACKTRACE
        CUBIT_SYNCHRONIZED_SCOPE
        if (started || !hasFunction) {
            return;
        }
        started = true;
        threadID = generateThreadID();
        internalThread = std::thread([&]() {
            CUBIT_STACKTRACE
            thisThread.attach(selfPtr.lock());
            {
                std::unique_lock lock{vectorMutex};
                runningThreads.emplace_back(thisThread);
            }
            try {
                function();
            } catch (std::exception e) {
                // if an exception made it this far back, ya fucked up a-a-ron
                Logging::fatal(e.what());
                for (auto& handler : this->uncaughtExceptionHandlers) {
                    handler(e);
                }
                for (auto& handler : Threading::uncaughtExceptionHandlers) {
                    handler(e);
                }
                // allows a handler to take responsibility for the exception
                if (shouldTerminateAtException) {
                    std::terminate();
                }
            }
            {
                // remove this from running threads, as its about to stop
                std::unique_lock lock{vectorMutex};
                auto iterator = runningThreads.begin();
                do {
                    if (iterator.operator*().id() == id()) {
                        runningThreads.erase(iterator);
                        break;
                    }
                } while (++iterator != runningThreads.end());
            }
            thisThread.detach();
            threadID = std::numeric_limits<uint64_t>::max();
            started = false;
        });
    }
    
    bool Thread::IMPL::joinable() {
        return started && internalThread.joinable();
    }
    
    void Thread::IMPL::join() {
        CUBIT_STACKTRACE
        joined = true;
        internalThread.join();
    }
    
    uint64_t Thread::IMPL::id() {
        return threadID;
    }
    
    void Thread::IMPL::setSelfPtr(std::shared_ptr<Thread::IMPL> ptr) {
        if (ptr.get() != this) {
            // the fuck are you trying to do?
            return;
        }
        selfPtr = ptr;
    }
    
    void Thread::IMPL::terminateAtException(bool shouldTerminate) {
        shouldTerminateAtException = shouldTerminate;
    }
    
    void Thread::IMPL::registerThreadSpecificUncaughtExceptionHandler
            (std::function<void(std::exception exception)> handler) {
        this->uncaughtExceptionHandlers.emplace_back(handler);
    }
    
    Thread::Thread() {
        CUBIT_STACKTRACE
        impl = std::make_shared<IMPL>();
    }
    
    Thread::Thread(std::function<void()> function) {
        CUBIT_STACKTRACE
        impl = std::make_shared<IMPL>(function);
        impl->setSelfPtr(impl);
    }
    
    Thread::Thread(std::shared_ptr<Thread::IMPL> impl) {
        CUBIT_STACKTRACE
        this->impl = impl;
    }
    
    void Thread::start() {
        impl->start();
    }
    
    bool Thread::joinable() {
        return impl->joinable();
    }
    
    void Thread::join() {
        CUBIT_STACKTRACE
        impl->join();
    }
    
    uint64_t Thread::id() {
        return impl->id();
    }
    
    int Thread::hardwareConcurrency() {
        return std::thread::hardware_concurrency();
    }
    
    int Thread::physicalConcurrency() {
        return boost::thread::physical_concurrency();
    }
    
    Thread Thread::currentThread() {
        return Threading::thisThread;
    }
    
    void Thread::attach(std::shared_ptr<Thread::IMPL> impl) {
        this->impl = impl;
    }
    
    void Thread::detach() {
        impl = nullptr;
    }
    
    void Thread::registerUncaughtExceptionHandler(std::function<void(std::exception)> handler) {
        CUBIT_STACKTRACE
        CUBIT_SYNCHRONIZED_SCOPE
        uncaughtExceptionHandlers.push_back(handler);
    }
    
    void Thread::joinAll() {
        std::vector<Thread> threads;
        {
            std::unique_lock lock{vectorMutex};
            threads = runningThreads;
        }
        for (auto& thread : threads) {
            if (thread.joinable()) {
                thread.join();
            }
        }
    }
    
    void Thread::terminateAtException(bool shouldTerminate) {
        impl->terminateAtException(shouldTerminate);
    }
    
    void Thread::registerThreadSpecificUncaughtExceptionHandler(std::function<void(std::exception exception)> handler) {
        impl->registerThreadSpecificUncaughtExceptionHandler(handler);
    }
    
    OutOfThreadIDs::OutOfThreadIDs(
            const char* function,
            const char* file,
            int linenum,
            const std::string& message, std::string
            errorType) : FatalError(
            function, file, linenum, message, errorType) {}
}