#include "Cubit/Libraries/Threading/Queue.hpp"
#include <Cubit/Libraries/cpponmulticore/sema.h>
#include <mutex>
#include <boost/bind.hpp>
#include <Cubit/Libraries/Threading/DestructorCallback.hpp>
#include <Cubit/Libraries/Threading/Queue.hpp>
#include <iostream>


namespace Cubit::Libraries::Threading {
    static thread_local bool waiting = false;
    class Queue::IMPL {
        std::mutex accessMutex;
        std::list<Item> queue;
        LightweightSemaphore dequeueSemaphore;
        std::atomic_bool destroyed = false;
        std::atomic_int waitingThreads = 0;
    public:
        Event enqueue(Item item);
        
        Dequeue dequeue();
        
        Item dequeueItem(std::shared_ptr<IMPL>& ptr);
        
        std::weak_ptr<IMPL> selfPtr;
        
        ~IMPL();
    };
    
    Event Queue::IMPL::enqueue(Item item) {
        item.whenReady(boost::bind<void>([](std::shared_ptr<IMPL> queue, Item toEnqueue) {
            std::unique_lock lk(queue->accessMutex);
            queue->queue.push_back(toEnqueue);
            queue->dequeueSemaphore.signal();
        }, selfPtr.lock(), item));
        return item.event();
    }
    
    Queue::Item Queue::IMPL::dequeueItem(std::shared_ptr<IMPL>& ptr) {
        waitingThreads++;
        waiting = true;
        DestructorCallback waitCallback([&]() {
            waitingThreads--;
            waiting = false;
        });
        // the destructor will know we are here, its ok to release our lock
        ptr.reset();
        // make sure we didnt just destroy the object
        if (!waiting) { // cant access object address space, it may be deconstructed
            return {{[]() {}}};
        }
        dequeueSemaphore.wait();
        if (destroyed) {
            return {{[]() {}}};
        }
        std::unique_lock lk(accessMutex);
        Item item = queue.front();
        queue.pop_front();
        lk.unlock();
        return item;
    }
    
    Queue::Dequeue Queue::IMPL::dequeue() {
        return {selfPtr};
    }
    
    Queue::IMPL::~IMPL() {
        destroyed = true;
        dequeueSemaphore.signal(std::numeric_limits<int>::max());
        // if this is being called from a dequeue release, there will be one left waiting.
        while (waitingThreads > waiting ? 1 : 0);
        waiting = false;
    }
}

namespace Cubit::Libraries::Threading {
    
    class Queue::Dequeue::IMPL {
        std::weak_ptr<Queue::IMPL> queue;
    public:
        IMPL(std::weak_ptr<Queue::IMPL> ptr);
        
        Item dequeue();
    };
    
    Queue::Item Queue::Dequeue::IMPL::dequeue() {
        auto queue = this->queue.lock();
        if (queue) {
            return queue->dequeueItem(queue);
        }
        return {{[]() {}}};
    }
    
    Queue::Dequeue::IMPL::IMPL(std::weak_ptr<Queue::IMPL> ptr) {
        queue = ptr;
    }
}

namespace Cubit::Libraries::Threading {
    class Queue::Item::IMPL {
        Event event;
        boost::function<void()> function;
        std::mutex eventReadyMutex;
        std::atomic_uint64_t untriggeredWaitEvents;
        Event readyEvent;
    public:
        std::weak_ptr<IMPL> selfPtr;
        
        // selfptr is invalid in the constructor, can i need that
        void setVals(boost::function<void()> function, std::vector<Event> waitEvents);
        
        void whenReady(std::function<void()> callback);
        
        bool ready();
        
        Event waitEvent();
        
        void process();
    };
    
    void Queue::Item::IMPL::setVals(boost::function<void()> function, std::vector<Event> waitEvents) {
        this->function = function;
        if (waitEvents.empty()) {
            readyEvent.trigger();
            return;
        }
        untriggeredWaitEvents = waitEvents.size();
        for (auto& waitEvent : waitEvents) {
            waitEvent.registerCallback(boost::bind<void>([](std::shared_ptr<IMPL> item) {
                std::unique_lock lk(item->eventReadyMutex);
                item->untriggeredWaitEvents--;
                if (item->untriggeredWaitEvents == 0) {
                    lk.unlock();
                    item->readyEvent.trigger();
                }
            }, selfPtr.lock()));
        }
    }
    
    void Queue::Item::IMPL::whenReady(std::function<void()> callback) {
        readyEvent.registerCallback(callback);
    }
    
    bool Queue::Item::IMPL::ready() {
        return untriggeredWaitEvents == 0;
    }
    
    Event Queue::Item::IMPL::waitEvent() {
        return event;
    }
    
    void Queue::Item::IMPL::process() {
        readyEvent.wait();
        function();
        event.trigger();
    }
}

namespace Cubit::Libraries::Threading {
    Queue::Queue() {
        impl = std::make_shared<IMPL>();
        impl->selfPtr = impl;
    }
    
    Event Queue::enqueue(boost::function<void()> function, std::vector<Event> waitEvents) {
        return impl->enqueue({function, waitEvents});
    }
    
    Queue::Dequeue Queue::dequeue() {
        return impl->dequeue();
    }
    
    void Queue::null() {
        impl = nullptr;
    }
    
    Queue::Item::Item(boost::function<void()> function, std::vector<Event> waitEvents) {
        impl = std::make_shared<IMPL>();
        impl->selfPtr = impl;
        impl->setVals(function, waitEvents);
    }
    
    void Queue::Item::whenReady(std::function<void()> callback) {
        impl->whenReady(callback);
    }
    
    bool Queue::Item::ready() {
        return impl->ready();
    }
    
    Event Queue::Item::event() {
        return impl->waitEvent();
    }
    
    void Queue::Item::process() {
        impl->process();
    }
    
    Queue::Dequeue::Dequeue(std::weak_ptr<Queue::IMPL> ptr) {
        impl = std::make_shared<IMPL>(ptr);
    }
    
    Queue::Item Queue::Dequeue::dequeue() {
        return impl->dequeue();
    }
}