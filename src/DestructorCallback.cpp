#include "Cubit/Libraries/Threading/DestructorCallback.hpp"
namespace Cubit::Libraries::Threading{
    DestructorCallback::DestructorCallback(std::function<void()> function) : function(std::move(function)) {
    }

    DestructorCallback::DestructorCallback() {
        function = nullptr;
    }

    DestructorCallback::~DestructorCallback() {
        if (function != nullptr) {
            function();
        }
    }

    DestructorCallback& DestructorCallback::operator=(std::function<void()> function) {
        function = std::move(function);
        return *this;
    }
}